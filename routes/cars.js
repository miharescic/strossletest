// cars.js

var Car = require('../models/car');

/**
 * GET /cars {id}
 */
exports.get = function(req, res, next) {
	var id = req.query.id;
	var params = {};
	params.fields = req.query.fields;
	params.limit = req.query.limit;
	params.offset = req.query.offset;
	params.filter = req.query.filter;

	Car.get(id, params, function(err, cars) {
		if (err) return next(err);

		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify(cars));
	});
};

/**
 * DELETE /cars {id}
 */
exports.delete = function(req, res, next) {
	var id = req.query.id;

	Car.delete(id, function(err, cars) {
		if (err) return next(err);

		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify({
			result: "success"
		}));
	});
};


/**
 * POST /cars {data}
 */
exports.add = function(req, res, next) {

	var data = req.body;
	console.log(data);

	Car.add(data, function(err, cars) {
		if (err) return next(err);

		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify(cars));
	});
};

/**
 * PUT /cars {id}
 */
exports.update = function(req, res, next) {
	console.log('Car.update: ');

	var id = req.query.id;
	var data = req.body;

	Car.update(id, data, function(err, cars) {
		if (err) return next(err);

		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify(cars));
	});
};

