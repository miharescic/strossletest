var neo4j = require('neo4j');
var db = new neo4j.GraphDatabase('http://neo4j:password@localhost:7474');

var Car = module.exports = function() {};

var queryFields = 'c.id, c.manufacturer, c.model, c.year, c.color';

// Get all the cars or a specific car with ID
Car.get = function(id, params, callback) {
	var queryLimit = '';
	var	query = '';
	
	if (typeof params.limit != 'undefined') {
		queryLimit = ' LIMIT ' + params.limit;
	}

	var queryOffset = '';
	if (typeof params.offset != 'undefined') {
		queryOffset = ' SKIP ' + params.offset;
	}

	var queryFilter = '';
	if (typeof params.filter != 'undefined') {

		var filter = params.filter.split('=');

		queryFilter = ' WHERE c.' + filter[0] + '=\'' + filter[1] + '\'';
	}


	if (typeof params.fields != 'undefined') {
		var entries = params.fields.split(',');
		queryFields = '';

		for (var i = 0, l = entries.length; i < l; i++) {
			queryFields = queryFields + 'c.' + entries[i];
			if (i < l - 1) queryFields += ', ';
		}
	}


	if (typeof id === 'undefined') {
		query = 'MATCH (c:Car) ' + queryFilter + ' RETURN ' + queryFields + queryLimit + queryOffset;

		db.cypher({
			query: query
		}, function(err, results) {
			if (err) return callback(err);

			callback(null, results);
		});
	} else {
		query = 'MATCH (c:Car {id: {id}}) RETURN ' + queryFields;

		var param = {
			id: id,
		};

		db.cypher({
			query: query,
			params: param,
		}, function(err, results) {
			if (err) return callback(err);
			if (!results.length) {
				err = new Error('No database entry with id: ' + id);
				return callback(err);
			}
			callback(null, results);
		});
	}
};

// Remove a car from the database
Car.delete = function(id, callback) {
	var query = 'MATCH (c:Car {id: {id}}) DELETE c';

	var params = {
		id: id,
	};

	db.cypher({
		query: query,
		params: params,
	}, function(err, results) {
		if (err) return callback(err);

		callback(null, results);
	});
};

// Add a car to the database
Car.add = function(data, callback) {
	console.log('Car.add: ');

	var query = 'MERGE (c:Car {id: {id}, manufacturer: {manufacturer}, model: {model}, year: {year}, color: {color}}) RETURN ' + queryFields;

	var params = {
		id: data.id,
		manufacturer: data.manufacturer,
		model: data.model,
		year: data.year,
		color: data.color
	};

	db.cypher({
		query: query,
		params: params,
	}, function(err, results) {
		if (err) return callback(err);
		if (results.length != 1) {
			err = new Error('Error inserting car: with data:  ' + data);
			return callback(err);
		}
		callback(null, results);
	});

};

// Update an existing car in the database
Car.update = function(id, data, callback) {
	console.log('Car.update: ');

	var query = 'MATCH (c:Car {id: {id}}) SET c.manufacturer = {manufacturer}, c.model = {model}, c.year = {year}, c.color = {color} RETURN ' + queryFields;

	var params = {
		id: id,
		manufacturer: data.manufacturer,
		model: data.model,
		year: data.year,
		color: data.color
	};

	db.cypher({
		query: query,
		params: params,
	}, function(err, results) {
		if (err) return callback(err);
		if (results.length != 1) {
			err = new Error('Error updating car: ' + id + ', ' + data);
			return callback(err);
		}
		callback(null, results);
	});

};