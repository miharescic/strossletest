var express = require('express');
var routes = require('./routes');

var app = express();

// Enable JSON parsing
var bodyParser = require('body-parser');
app.use(bodyParser.json());

// Enable logging
var logger = require('express-logger');
app.use(logger({
	path: "./logs/logfile.txt"
}));

// Log response times
var expressMetrics = require('express-metrics');

// Show metrics to user
app.use(expressMetrics({
	port: 4000
}));

// Use really basic authentication
var auth = require('basic-auth');

var _username = 'miharescic';
var _password = 'strossle';

// Route all cars traffic trough authentication service
app.all('/cars', function(req, res, next) {
	var credentials = auth(req);

	if (credentials.name === _username && credentials.pass === _password) {
		next();
	} else {
		next(new Error(401)); // 401 Not Authorized
	}
});

// Handle all the database routes
app.get('/cars', routes.cars.get);
app.delete('/cars', routes.cars.delete);
app.post('/cars', routes.cars.add);
app.put('/cars', routes.cars.update);

// Start server on port 3000
app.listen(3000, function() {
	console.log('Server started, listening on port 3000!');
});